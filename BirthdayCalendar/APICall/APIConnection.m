//
//  APIConnection.m
//  APICall
//
//  Created by Alexander Kholodov on 05.02.11.
//  Copyright 2011 Yalantis. All rights reserved.
//

#import "APIConnection.h"


@implementation APIConnection

@synthesize data = _data;


#pragma mark -
#pragma mark NSURLConnection
- (id) initWithRequest: (NSURLRequest*)request delegate: (id)delegate
{
	if (self = [super initWithRequest:request delegate:delegate startImmediately:NO])
	{
		_data = [NSMutableData new];
	}
	return self;
}

#pragma mark -
- (void) appendData: (NSData*)data
{
	[_data appendData:data];
}


@end
