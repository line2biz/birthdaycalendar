//
//  APIRequest.m
//  APICall
//
//  Created by Alexander Kholodov on 05.02.11.
//  Copyright 2011 Yalantis. All rights reserved.
//

#import "APIRequest.h"


@implementation APIRequest


+(id)getRequestWithURL:(NSString*)urlString values:(NSDictionary*)values {
	
	urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	urlString = [urlString stringByAppendingString:@"?"];
	for (NSString *key in values)
	{
		NSString *value = [values valueForKey:key];
		urlString = [urlString stringByAppendingFormat:@"%@=%@&",
					 [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
					 [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	}
	
	NSURLRequest *_request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
	
	return _request;
}


+(id)postRequestWithURL:(NSString*)urlString values:(NSDictionary*)values delegate:(id)_delegate {
	
	NSMutableURLRequest *_request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
	
	[_request setHTTPMethod:@"POST"];
	NSMutableString *string = [NSMutableString new];
	for (NSString *key in values) {
		NSString *value = [values valueForKey:key];
		value = [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		[string appendFormat:@"%@=%@&", key, value];
	}
	NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
	[_request setHTTPBody:data];
	[string release];
	
	return _request;
}

@end
