//
//  APIConnection.h
//  APICall
//
//  Created by Alexander Kholodov on 05.02.11.
//  Copyright 2011 Yalantis. All rights reserved.
//

@interface APIConnection : NSURLConnection {
	NSMutableData *_data;
}

@property (nonatomic, readonly) NSMutableData *data;

-(void)appendData:(NSData*)data;

@end
