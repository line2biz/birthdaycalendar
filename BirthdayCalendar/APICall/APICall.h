//
//  APICall.h
//  APICall
//
//  Created by Alexander Kholodov on 05.02.11.
//  Copyright 2011 Yalantis. All rights reserved.
//

@class APICall;

typedef enum {
	GETRequest,
	POSTRequest
} HTTPMethod;

@interface APICall : NSObject {
	id delegate;
	NSURLRequest *request;
	NSInteger tag;
	SEL successSelector;
}

@property (nonatomic, readonly) id delegate;
@property (nonatomic, readonly) NSURLRequest *request;
@property (nonatomic, assign) NSInteger tag;

+(id)callWithURLString:(NSString *)urlString delegate:(id)delegate;
+(id)callWithURLString:(NSString *)urlString values:(NSDictionary *)values delegate:(id)delegate;
+(id)callWithURLString:(NSString *)urlString method:(HTTPMethod)method 
				  values:(NSDictionary *)values delegate:(id)delegate;

-(id)initWithURLString:(NSString *)urlString delegate:(id)delegate;
-(id)initWithURLString:(NSString *)urlString values:(NSDictionary *)values delegate:(id)delegate;
-(id)initWithURLString:(NSString *)urlString method:(HTTPMethod)method 
				  values:(NSDictionary *)values delegate:(id)delegate;

//Selector should conform to the next scheme: @selector(APICall:loadedSomeValue:)
//Default is @selector(APICall:didLoadValue:)
//Used to divide different types of concurent data loading (i.e divide images from account data etc)
-(void)setSuccessSelector:(SEL)selector withTag:(NSInteger)_tag;

-(void)start;

@end
