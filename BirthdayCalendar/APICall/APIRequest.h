//
//  APIRequest.h
//  APICall
//
//  Created by Alexander Kholodov on 05.02.11.
//  Copyright 2011 Yalantis. All rights reserved.
//


@interface APIRequest : NSURLRequest {

}

+(id)getRequestWithURL:(NSString*)urlString values:(NSDictionary*)values;
+(id)postRequestWithURL:(NSString*)urlString values:(NSDictionary*)values delegate:(id)_delegate;

@end
