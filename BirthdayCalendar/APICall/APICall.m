//
//  APICall.m
//  APICall
//
//  Created by Alexander Kholodov on 05.02.11.
//  Copyright 2011 Yalantis. All rights reserved.
//

#import "APICall.h"
#import "APIRequest.h"
#import "APIConnection.h"

@implementation APICall

@synthesize delegate;
@synthesize request;
@synthesize tag;

- (void) dealloc
{
	[request release];
	[delegate release];
	[super dealloc];
}

#pragma mark -
#pragma mark Initialization

+(id)callWithURLString:(NSString *)urlString delegate:(id)_delegate {
	return [APICall callWithURLString:urlString method:GETRequest values:nil delegate:_delegate];
}

+(id)callWithURLString:(NSString *)urlString values:(NSDictionary *)values delegate:(id)_delegate {
	return [APICall callWithURLString:urlString method:GETRequest values:values delegate:_delegate];
}

+(id)callWithURLString:(NSString *)urlString method:(HTTPMethod)method 
				  values:(NSDictionary *)values delegate:(id)_delegate {
	APICall *call = [[APICall alloc] initWithURLString:urlString method:method values:values delegate:(id)_delegate];
	return [call autorelease];
}

-(id)initWithURLString:(NSString *)urlString delegate:(id)_delegate {
	return [self initWithURLString:urlString method:GETRequest values:nil delegate:_delegate];
}

-(id)initWithURLString:(NSString *)urlString values:(NSDictionary *)values delegate:(id)_delegate {
	return [self initWithURLString:urlString method:GETRequest values:values delegate:_delegate];
}

-(id)initWithURLString:(NSString *)urlString method:(HTTPMethod)method 
				  values:(NSDictionary *)values delegate:(id)_delegate {
	self = [super init];
	
	delegate = [_delegate retain];
	APIRequest *_request = (GETRequest == method) ? 
		[APIRequest getRequestWithURL:urlString values:values]
		: [APIRequest postRequestWithURL:urlString values:values delegate:self];
	
	request = [_request retain];;
	return self;
}


#pragma mark -
#pragma mark NSURLConnectionDelegate
- (void) connection: (APIConnection*)connection didReceiveData: (NSData*)data {
	[connection appendData:data];
}

- (void) connectionDidFinishLoading: (APIConnection*)connection {
	
#ifndef __OPTIMIZE__
	NSString *response = [[NSString alloc] initWithData:connection.data
											   encoding:NSUTF8StringEncoding];
	NSLog(@"Response:\n%@", response);
	[response release];
#endif
	
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

	if (!successSelector) {
		successSelector = @selector(APICall:didLoadValue:);
	}
	if ([delegate respondsToSelector:successSelector]) {
		[delegate performSelector:successSelector withObject:self withObject:connection.data];
	}
}

- (void) connection: (APIConnection*)connection didFailWithError: (NSError*)error {
	[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	
	SEL selector = @selector(APICall:didFailWithError:);
	if ([delegate respondsToSelector:selector]) {
		[delegate performSelector:selector withObject:self withObject:error];
	}
}


#pragma mark -

-(void)start {
	#ifndef __OPTIMIZE__
		NSString *body = [[NSString alloc] initWithData:[request HTTPBody]
											   encoding:NSUTF8StringEncoding];
		NSLog(@"%@\n\n%@", [request URL], body);
		[body release];
	#endif
		
	APIConnection *connection = [[APIConnection alloc] initWithRequest:request delegate:self];
	if (connection) {
		[connection start];
		[connection release];
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	}
}

-(void)setSuccessSelector:(SEL)selector withTag:(NSInteger)_tag {
	self.tag = _tag;
	successSelector = selector;
}

@end
