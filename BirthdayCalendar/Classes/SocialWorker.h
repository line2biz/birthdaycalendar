//
//  SocialWorker.h
//  BirthdayCalendar
//
//  Created by Alximik on 11.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import <AddressBook/AddressBook.h>
#import "MBProgressHUD.h"
#import "FBConnect.h"

@protocol SocialWorkerlDelegate;

@interface SocialWorker : NSObject <FBRequestDelegate, FBDialogDelegate, FBSessionDelegate> {
    id <SocialWorkerlDelegate> __weak delegate;
    
    MBProgressHUD *progressView;
    Facebook *facebook;
}
@property (nonatomic, weak)id <SocialWorkerlDelegate> delegate;

@property (nonatomic, strong) MBProgressHUD *progressView;
@property (nonatomic, strong) Facebook *facebook;

- (void)initFacebook;
- (void)loginToFacebook;
- (void)publishStream;
- (void)logoutFacebook;
- (void)syncFacebook;
- (void)syncAdressBook;
- (NSDate*)dateFromString:(NSString*)dateStr format:(NSString*)format;

+(SocialWorker *)sharedSocialWorker;

@end


@protocol SocialWorkerlDelegate<NSObject>

@optional
- (void)didLogin;
- (void)didLogout;
@end