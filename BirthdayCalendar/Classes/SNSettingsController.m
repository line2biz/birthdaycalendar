//
//  SettingsViewController.m
//  BirthdayCalendar
//
//  Created by Alximik on 04.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SNSettingsController.h"



@interface SNSettingsController ()
{
    __weak IBOutlet UISwitch *_notificationSwitch;
    __weak IBOutlet UISwitch *_facebookSwitch;
    __weak IBOutlet UILabel *_timeLabel;
    
    
}

@end

@implementation SNSettingsController

@synthesize sourcesArray;
@synthesize pickerViewDate;
@synthesize currentDateNotification;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = tableRowHeight;
    
    UIImageView *logoimage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BarLogo"]];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoimage];
    
    self.currentDateNotification = [[NSUserDefaults standardUserDefaults] objectForKey:dateNotification];
    
    self.sourcesArray = [[[NSUserDefaults standardUserDefaults] objectForKey:SocialNetworksKey] mutableCopy];
    
    NSLog(@"%s %@", __FUNCTION__, sourcesArray);
    
    [self configureView];
    
}

- (void)configureView
{
    _timeLabel.text = [SourcesManager timeString:currentDateNotification];
    _notificationSwitch.on = [[NSUserDefaults standardUserDefaults] boolForKey:notificationOnKey];
    
    NSDictionary *network = [sourcesArray objectAtIndex:1];
    _facebookSwitch.on = [[network objectForKey:@"activated"] isEqualToString:@"YES"];
}

#pragma mark - Outlet methods
- (IBAction)switchValueChanged:(UISwitch *)sender
{
    if (sender == _facebookSwitch) {
        NSMutableDictionary *sourcesObjects = [[sourcesArray objectAtIndex:1] mutableCopy];
        [sourcesObjects setObject:sender.on ? @"YES" : @"NO" forKey:@"activated"];
        [sourcesArray replaceObjectAtIndex:1 withObject:sourcesObjects];
        
        [[NSUserDefaults standardUserDefaults] setObject:sourcesArray forKey:SocialNetworksKey];
        
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:notificationOnKey];
        if (sender.on) {
            [SourcesManager cleanOldNotifications];
        } else {
            [SourcesManager addPushNotifications:[SourcesManager loadSources]];
        }
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}



-(void)switchDidChange:(id)sender {
	
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	UISwitch *mySwitch = (UISwitch *)sender;
    
	if (mySwitch.tag) {
        NSMutableDictionary *sourcesObjects = [[sourcesArray objectAtIndex:mySwitch.tag] mutableCopy];
        [sourcesObjects setObject:mySwitch.on ? @"YES" : @"NO" forKey:@"activated"];
        [sourcesArray replaceObjectAtIndex:mySwitch.tag withObject:sourcesObjects];
        
        [userDefaults setObject:sourcesArray forKey:SocialNetworksKey];
    } else {
        [userDefaults setBool:mySwitch.on forKey:notificationOnKey];
        if (mySwitch.on) {
            [SourcesManager cleanOldNotifications];
        } else {
            [SourcesManager addPushNotifications:[SourcesManager loadSources]];
        }
    }
    
    [userDefaults synchronize];
}

#pragma mark - Table view data source
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return [sourcesArray count];
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    return cell;
//    
//    
//	EditSourcesCell *cell = (EditSourcesCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EditSourcesCell" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//	}
//    
//    NSDictionary *network = [sourcesArray objectAtIndex:indexPath.row];
//    
//    cell.nameSources.text = [network objectForKey:@"name"];
//    
//    [cell.mySwitch addTarget:self action:@selector(switchDidChange:) forControlEvents:UIControlEventTouchUpInside];
//    cell.mySwitch.tag = indexPath.row;
//    
//    if (indexPath.row == positionNotification) {
//        cell.time.text = [SourcesManager timeString:currentDateNotification];
//        [cell.mySwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:notificationOnKey] animated:NO];
//    } else {
//        [cell.mySwitch setOn:[[network objectForKey:@"activated"] isEqualToString:@"YES"] animated:NO];
//    }
//    
//    return cell;
//}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    
    if (indexPath.row == positionNotification) {
        self.pickerViewDate = [[UIActionSheet alloc] initWithTitle:@"Notification"
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:nil];
        
        UIDatePicker *theDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0f, 44.0f, 0.0f, 0.0f)];
        theDatePicker.datePickerMode = UIDatePickerModeCountDownTimer;
        [theDatePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
        theDatePicker.date = [NSDate date];
        
        UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)];
        pickerToolbar.barStyle=UIBarStyleBlackOpaque;
        [pickerToolbar sizeToFit];   
        NSMutableArray *barItems = [[NSMutableArray alloc] init];   
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeDatePicker)];
        [barItems addObject:flexSpace];
        
        [pickerToolbar setItems:barItems animated:YES];       
        [pickerViewDate addSubview:pickerToolbar];
        [pickerViewDate addSubview:theDatePicker];
        [pickerViewDate showFromTabBar:self.tabBarController.tabBar];
        [pickerViewDate setBounds:CGRectMake(0.0f, 0.0f, 320.0f, 464.0f)];        
    } else {
        NSDictionary *network = [sourcesArray objectAtIndex:indexPath.row];
        if ([[network objectForKey:@"activated"] isEqualToString:@"YES"]) {
            SNSocialController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNSocialController class])];
            detailViewController.socialName = [network objectForKey:@"name"];
            [self.navigationController pushViewController:detailViewController animated:YES];
        }        
    }
}

-(void)closeDatePicker {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:currentDateNotification forKey:dateNotification];
    [userDefaults synchronize];
    [self configureView];
    
    [pickerViewDate dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)dateChanged:(id)sender {
    self.currentDateNotification = [sender date];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
