//
//  SocialWorker.m
//  BirthdayCalendar
//
//  Created by Alximik on 11.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SocialWorker.h"

@implementation SocialWorker

@synthesize delegate;
@synthesize facebook;
@synthesize progressView;

static SocialWorker * sharedSocialWorker = NULL;
+ (SocialWorker *) sharedSocialWorker {
	if ( !sharedSocialWorker || sharedSocialWorker == NULL ) {
		sharedSocialWorker = [SocialWorker new];
	}
	
	return sharedSocialWorker;
}


- (void)syncAdressBook {
    NSMutableArray *contactsAB = [SourcesManager loadSources];
    
    
    CFErrorRef error = nil;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
#if DEBUG
    if (error) { NSLog(@"%@", error); }
#endif
    
    __block BOOL accessGranted = NO;
    
    
    if (ABAddressBookRequestAccessWithCompletion != NULL) { // we're on iOS 6
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        });
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    
    if (accessGranted) {
        
//        NSArray *thePeople = (__bridge_transfer NSArray*)ABAddressBookCopyArrayOfAllPeople(addressBook);
        // Do whatever you need with thePeople...
        
        
        
        NSArray *peopleInPhone = (NSArray*)CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(addressBook));
        
        for (int i=0; i<[peopleInPhone count]; i++) {
            ABRecordRef person = (__bridge ABRecordRef)([peopleInPhone objectAtIndex:i]);
            
            ABMultiValueRef phones =(__bridge ABMultiValueRef)((NSString*)CFBridgingRelease(ABRecordCopyValue(person, kABPersonPhoneProperty)));
            NSString *mobile = nil;
            NSString *mobileLabel = nil;
            for(CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
                mobileLabel = (NSString*)CFBridgingRelease(ABMultiValueCopyLabelAtIndex(phones, i));
                if([mobileLabel isEqualToString:@"_$!<Mobile>!$_"]) {
                    mobileLabel = nil;
                    mobile = (NSString*)CFBridgingRelease(ABMultiValueCopyValueAtIndex(phones, i));
                }
            }
            
            NSString *idContact = mobile? mobile:mobileLabel;
            if (idContact) {
                CFStringRef firstName = ABRecordCopyValue(person, kABPersonFirstNameProperty);
                CFStringRef lastName = ABRecordCopyValue(person, kABPersonLastNameProperty);
                CFStringRef personBirthday = ABRecordCopyValue(person, kABPersonBirthdayProperty);
                
                NSString *firstNameStr = @"";
                if (firstName) {
                    firstNameStr = [NSString stringWithFormat:@"%@", firstName];
                }
                NSString *lastNameStr = @"";
                if (lastName) {
                    lastNameStr = [NSString stringWithFormat:@"%@", lastName];
                }
                
                NSDate *birthday = nil;
                NSString *dateStr = [NSString stringWithFormat:@"%@", personBirthday];
                if (![dateStr isEqualToString:@"(null)"]) {
                    dateStr = [dateStr substringToIndex:10];
                    birthday = [self dateFromString:dateStr format:@"yyyy-MM-dd"];
                }
                
                [SourcesManager addContactToApp:contactsAB
                                      idContact:idContact
                                           name:[NSString stringWithFormat:@"%@ %@", firstNameStr, lastNameStr]
                                        picture:@""
                                     sourceName:@"AB"
                                       birthday:birthday];
                
                if(ABPersonHasImageData(person)){
                    NSData *imageData = (NSData*)CFBridgingRelease(ABPersonCopyImageData(person));
                    NSString *fileName = [NSString stringWithFormat:@"%@_%@.jpg", @"AB", idContact];
                    [SourcesManager saveImageForName:imageData name:fileName];
                }
            }
            
        }
        
    }
    
    
    [SourcesManager saveSources:contactsAB];
}

- (void)initFacebook {
    if (!facebook) {
        self.facebook = [[Facebook alloc] initWithAppId:FBAppId];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        self.facebook.accessToken = [userDefaults objectForKey:@"AccessToken"];
        self.facebook.expirationDate = [userDefaults objectForKey:@"ExpirationDate"];
        
    }
}

- (void)loginToFacebook {
    [self initFacebook];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FBpermissions" ofType:@"plist"];
    NSDictionary *settingsDic = [NSDictionary dictionaryWithContentsOfFile:path];
    NSArray *permissions =   [settingsDic objectForKey:@"facebookPermissions"];
    [facebook authorize:permissions delegate:self];
}

- (void)publishStream {
    [self initFacebook];
    SBJSON *jsonWriter = [SBJSON new];
    
    NSDictionary* actionLinks = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           @"Always Running",@"text",@"http://itsti.me/",@"href", nil], nil];
    
    NSString *actionLinksStr = [jsonWriter stringWithObject:actionLinks];
    NSDictionary* attachment = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"a long run", @"name",
                                @"The Facebook Running app", @"caption",
                                @"it is fun", @"description",
                                @"http://itsti.me/", @"href", nil];
    NSString *attachmentStr = [jsonWriter stringWithObject:attachment];
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Share on Facebook",  @"user_message_prompt",
                                   actionLinksStr, @"action_links",
                                   attachmentStr, @"attachment",
                                   nil];
    
    
    [facebook dialog:@"feed"
           andParams:params
         andDelegate:self];
    
}

- (void)logoutFacebook {
    [facebook logout:self];
}

- (void)fbDidLogin {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:self.facebook.accessToken forKey:@"AccessToken"];
	[userDefaults setObject:self.facebook.expirationDate forKey:@"ExpirationDate"];
	[userDefaults synchronize];
	
	[facebook requestWithGraphPath:@"me" andDelegate:self];
}

- (void)fbDidLogout {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults removeObjectForKey:@"AccessToken"];
	[userDefaults removeObjectForKey:@"ExpirationDate"];
	[userDefaults removeObjectForKey:APInameFacebook];
	[userDefaults synchronize];
    
    SEL selector = @selector(didLogout);
	if (delegate && [delegate respondsToSelector:selector]) {
		[delegate performSelector:selector];
	}
}

- (void)request:(FBRequest *)request didLoad:(id)result {
    [progressView hide:YES];
    
    if ([result objectForKey:@"name"]) {
        NSUserDefaults	*defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[result objectForKey:@"name"] forKey:APInameFacebook];
        [defaults synchronize];
        
        SEL selector = @selector(didLogin);
        if (delegate && [delegate respondsToSelector:selector]) {
            [delegate performSelector:selector];
        }
    } else if ([result objectForKey:@"data"]) {
        NSArray *curentResult = [result objectForKey:@"data"];
        
        NSMutableArray *appContactsFB = [SourcesManager loadSources];
        
        for (NSDictionary *element in curentResult) {
            NSDate *birthday = [self dateFromString:[element objectForKey:@"birthday"] format:@"MM/dd/yyyy"];
            
            [SourcesManager addContactToApp:appContactsFB 
                                  idContact:[element objectForKey:@"id"] 
                                       name:[element objectForKey:@"name"]
                                    picture:[element objectForKey:@"picture"]
                                 sourceName:@"FB"
                                   birthday:birthday];
        }
        [SourcesManager saveSources:appContactsFB];
    }
}

- (void)syncFacebook {
    [self initFacebook];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:APInameFacebook]) {
        UIWindow* window = [UIApplication sharedApplication].keyWindow;
        if (!window) {
            window = [[UIApplication sharedApplication].windows objectAtIndex:0];
        }
        
        self.progressView = [[MBProgressHUD alloc] initWithView:window];
        [window addSubview:progressView];
        [progressView show:YES];
        
        [facebook requestWithGraphPath:@"me/friends?fields=id,name,picture,birthday" andDelegate:self];        
    } else {
        [self loginToFacebook];
    }
}

- (NSDate*)dateFromString:(NSString*)dateStr format:(NSString*)format {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:format];
    return [dateFormatter dateFromString:dateStr];
}

@end
