//
//  SNAppDelegate.m
//  BirthdayCalendar
//
//  Created by Шурик on 10.03.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import "SNAppDelegate.h"


@implementation SNAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:SocialNetworksKey]) {
        [SourcesManager loadSocialNetworksSettings];
    }
    return YES;
}

@end
