//
//  CalendarViewController.h
//  BirthdayCalendar
//
//  Created by Alximik on 06.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TapkuLibrary.h"
#import "APICall.h"

@interface SNCalendarController : TKCalendarMonthTableViewController {
    NSMutableArray *dataArray; 
	NSMutableDictionary *dataDictionary;
    NSArray *contacts;
}

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSMutableDictionary *dataDictionary;
@property (nonatomic, strong) NSArray *contacts;

- (void)createDataForCalendar:(NSDate*)start endDate:(NSDate*)end;
- (UIImage*)photoForContact:(NSDictionary*)curentContact;
- (void)startDownloadFile:(NSString*)URL index:(NSInteger)tag;
- (NSString*)photoName:(NSDictionary*)curentContact;

@end
