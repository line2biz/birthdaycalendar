//
//  EditViewContriller.h
//  BirthdayCalendar
//
//  Created by Alximik on 07.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface SNProfileContriller : UIViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate> {
}

@property (nonatomic, assign) BOOL editing;
@property (nonatomic, strong) UIBarButtonItem *editButton;
@property (nonatomic, strong) NSMutableArray *contacts;
@property (nonatomic, strong) NSMutableDictionary *curentContact;
@property (nonatomic, assign) NSInteger curentIndex;

@property (nonatomic, strong) UIActionSheet *birthdayAction;
@property (nonatomic, strong) UIActionSheet *pickerViewDate;



@end
