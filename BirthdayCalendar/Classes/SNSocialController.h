//
//  DetailSocialViewController.h
//  BirthdayCalendar
//
//  Created by Alximik on 14.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


@interface SNSocialController : UIViewController <SocialWorkerlDelegate> {
    NSString *socialName;
    UIButton *syncButton;
    UILabel *userNameLabel;
    UIButton *logButton;
}

@property (nonatomic, strong) NSString *socialName;
@property (nonatomic, strong) IBOutlet UIButton *syncButton;
@property (nonatomic, strong) IBOutlet UILabel *userNameLabel;
@property (nonatomic, strong) IBOutlet UIButton *logButton;

- (void)nameForObjects;
- (IBAction)pressLog;
- (IBAction)pressSync;

@end
