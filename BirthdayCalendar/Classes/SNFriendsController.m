//
//  ListViewController.m
//  BirthdayCalendar
//
//  Created by Alximik on 06.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "SNFriendsController.h"

#import "SNProfileContriller.h"

@implementation SNFriendsController

@synthesize contacts;


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *logoimage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BarLogo"]];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoimage];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                target:self 
                                                                                action:@selector(addContact)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    self.tableView.rowHeight = tableRowHeight;    
    self.clearsSelectionOnViewWillAppear = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    self.contacts = [SourcesManager loadSources];
    [self.tableView reloadData];
}

- (void)addContact {
    SNProfileContriller *editViewContriller = [[SNProfileContriller alloc] initWithNibName:@"EditViewContriller" bundle:nil];
    editViewContriller.editing = YES;
    [self.navigationController pushViewController:editViewContriller animated:YES];
}

#pragma mark - Segue Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[SNProfileContriller class]]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        if (indexPath) {
            [[segue destinationViewController] setEditing:NO];
            [[segue destinationViewController] setCurentIndex:indexPath.row];
            [[segue destinationViewController] setCurentContact:contacts[indexPath.row]];
            
        } else {
            [[segue destinationViewController] setEditing:YES];
            
        }
        

    }
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSDictionary *curentContact = [contacts objectAtIndex:indexPath.row];
    cell.textLabel.text = [curentContact objectForKey:@"name"];
    cell.detailTextLabel.text = [SourcesManager dateString:[curentContact objectForKey:@"birthday"] style:kCFDateFormatterLongStyle];
    
    UIImage *photoImage = [self photoForContact:curentContact];
    cell.imageView.image = [photoImage imageScaledToFitSize:CGSizeMake(50.0f, 50.0f)];
    
    [SourcesManager roundView:cell.imageView];
    
    return cell;
}

- (UIImage*)photoForContact:(NSDictionary*)curentContact {
    UIImage *photo = [SourcesManager loadImage:curentContact];
    
    if (!photo) {
        NSString *imageURL = [curentContact valueForKeyPath:@"picture.data.url"];
        if ([imageURL length]) {
            photo = [UIImage imageNamed:@"notPhoto.png"];
            [self startDownloadFile:imageURL index:[contacts indexOfObject:curentContact]];
        } else {
            photo = [UIImage imageNamed:@"notPhoto.png"];
        }
    }
    
    return photo;
}

- (void)startDownloadFile:(NSString*)URL index:(NSInteger)tag {
    APICall *request = [APICall callWithURLString:URL delegate:self];
    request.tag = tag;
    [request start];
}

- (void) APICall:(APICall *)request didLoadValue:(NSData *)data {
    NSDictionary *curentContact = [contacts objectAtIndex:request.tag];
    [SourcesManager saveImage:data contact:curentContact];
    
    [self.tableView reloadData];
}



@end