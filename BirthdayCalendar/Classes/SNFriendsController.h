//
//  ListViewController.h
//  BirthdayCalendar
//
//  Created by Alximik on 06.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "APICall.h"

@interface SNFriendsController : UITableViewController {
    NSArray *contacts;
}

@property (nonatomic, strong) NSArray *contacts;

- (UIImage*)photoForContact:(NSDictionary*)curentContact;
- (void)startDownloadFile:(NSString*)URL index:(NSInteger)tag;

@end
