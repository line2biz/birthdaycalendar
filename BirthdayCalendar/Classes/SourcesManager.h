//
//  SourcesManager.h
//  ImageViewer
//
//  Created by Alximik on 18.01.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIImage+ProportionalFill.h"

@interface SourcesManager : NSObject {

}

+(void)loadSocialNetworksSettings;
+(NSMutableArray*)loadSources;
+(void)saveSources:(NSMutableArray*)sourcesArray;
+(UIImage*)loadImage:(NSDictionary*)curentContact;
+(void)saveImage:(NSData*)dataImage contact:(NSDictionary*)curentContact;
+(void)saveImageForName:(NSData*)dataImage name:(NSString*)name;
+ (void)addContactToApp:(NSMutableArray*)contacts idContact:(NSString*)idContact name:(NSString*)name picture:(NSString*)picture sourceName:(NSString*)sourceName birthday:(NSDate*)birthday;
+ (NSString*)photoName:(NSDictionary*)curentContact;
+ (void)roundView:(UIView*)view;
+ (NSString*)dateString:(NSDate*)date style:(CFDateFormatterStyle)dateStyle;
+ (NSString*)timeString:(NSDate*)date;
+ (NSDate*)currentBirthday:(NSDate*)birthday inYear:(NSDate*)year;
+ (NSDate*)nextBirthday:(NSDate*)birthday;
+ (NSDate*)fireDate:(NSDate*)birthday;
+ (void)cleanOldNotifications;
+ (void)addPushNotifications:(NSArray*)contacts;
+ (NSString*)formAlertBody:(NSArray*)contacts;
+ (NSInteger)calculationOld:(NSDate*)birthday toDate:(NSDate*)toDate;

@end
