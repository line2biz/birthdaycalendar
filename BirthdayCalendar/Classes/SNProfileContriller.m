//
//  EditViewContriller.m
//  BirthdayCalendar
//
//  Created by Alximik on 07.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SNProfileContriller.h"

@interface SNProfileContriller ()

@property (nonatomic, strong) IBOutlet UILabel *monthLabel;
@property (nonatomic, strong) IBOutlet UILabel *dayLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UITextField *nameTextField;
@property (nonatomic, strong) IBOutlet UITextField *alertTextField;
@property (nonatomic, strong) IBOutlet UIButton *birthdayButton;
@property (nonatomic, strong) IBOutlet UIButton *commentButton;

@end


@implementation SNProfileContriller

- (void)viewDidUnload
{
    self.nameTextField = nil;
    self.monthLabel = nil;
    self.dayLabel= nil;
    self.imageView = nil;
    self.birthdayButton = nil;
    self.commentButton = nil;
    [super viewDidUnload];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Details";
    self.editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit"
                                                   style:UIBarButtonItemStylePlain 
                                                  target:self 
                                                  action:@selector(changeMode)];
    self.navigationItem.rightBarButtonItem = self.editButton;
    
    [SourcesManager roundView:self.imageView];
    
    [self visualizeMode];
    [self loadCurentContact];
    [self setTextValues];
    [self setPhotoImage];
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [self.curentContact setObject:self.nameTextField.text forKey:@"name"];
    [self hideKeyboard];
    return YES;
}

- (IBAction)changePhoto {
    [self hideKeyboard];
    
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Select cource" 
															delegate:self 
												   cancelButtonTitle:@"Cancel" 
											  destructiveButtonTitle:nil 
												   otherButtonTitles:@"Camera", @"Gallery", nil];
    
	popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showFromTabBar:self.tabBarController.tabBar];
}

- (IBAction)share {
    if ([MFMailComposeViewController canSendMail]) {
        [self displayComposerSheet];
    }
}

- (IBAction)shareFB {
    [[SocialWorker sharedSocialWorker] publishStream];
}

- (IBAction)changeBirthday {
    self.pickerViewDate = [[UIActionSheet alloc] initWithTitle:@"Birthday"
                                                 delegate:self
                                        cancelButtonTitle:nil
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:nil];
    
    UIDatePicker *theDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0f, 44.0f, 0.0f, 0.0f)];
    theDatePicker.datePickerMode = UIDatePickerModeDate;
    [theDatePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    NSDate *contactBirthday = [NSDate date];
    if ([self.curentContact objectForKey:@"birthday"]) {
        contactBirthday = [self.curentContact objectForKey:@"birthday"];
    }
    theDatePicker.date = contactBirthday;
    
    UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)];
    pickerToolbar.barStyle=UIBarStyleBlackOpaque;
    [pickerToolbar sizeToFit];   
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(closeDatePicker)];
    [barItems addObject:flexSpace];
    
    [pickerToolbar setItems:barItems animated:YES];       
    [self.pickerViewDate addSubview:pickerToolbar];
    [self.pickerViewDate addSubview:theDatePicker];
    [self.pickerViewDate showFromTabBar:self.tabBarController.tabBar];
    [self.pickerViewDate setBounds:CGRectMake(0.0f, 0.0f, 320.0f, 464.0f)];
}

- (IBAction)pressComment {
	UIAlertView * alert = [[ UIAlertView alloc	] initWithTitle:@"Enter comments:" message:@"  " 
													  delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	alert.transform = CGAffineTransformTranslate(alert.transform, 0.0f, 100.0f);
	[alert show];
    
	CGRect frame = alert.frame;
	frame.origin.y -= 100.0f;
	alert.frame = frame;
	self.alertTextField = [[UITextField alloc] initWithFrame:CGRectMake(20.0f, 50.0f, 245.0f, 25.0f)];
	self.alertTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.alertTextField.text = [self.curentContact objectForKey:@"comment"];
    self.alertTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
	[alert addSubview:self.alertTextField];
	[self.alertTextField becomeFirstResponder];
}

- (void)changeMode {
    if (self.editing) {
        self.editing = NO;
        [self saveAllValues];
    } else {
        self.editing = YES;
    }
    
    [self visualizeMode];
}

- (void)visualizeMode {
    if (self.editing) {
        [self.editButton setStyle:UIBarButtonItemStyleDone];
        [self.editButton setTitle:@"Done"];
        
        self.nameTextField.borderStyle = UITextBorderStyleRoundedRect;
    } else {
        [self.editButton setStyle:UIBarButtonItemStylePlain];
        [self.editButton setTitle:@"Edit"];
        
        self.nameTextField.borderStyle = UITextBorderStyleNone;
    }
    
    self.nameTextField.enabled = self.editing;
}

- (void)loadCurentContact {
    self.contacts = [SourcesManager loadSources];
    
    if (self.editing) {
        NSString *idContact = [[[NSDate date] description] stringByReplacingOccurrencesOfString:@":" withString:@"."];

        [SourcesManager addContactToApp:self.contacts
                              idContact:[idContact stringByReplacingOccurrencesOfString:@" +0000" withString:@""] 
                                   name:@""
                                picture:@""
                             sourceName:@"Custom"
                               birthday:[NSDate date]];
        
        self.curentIndex = [self.contacts count]-1;
    }
    
    self.curentContact = [[self.contacts objectAtIndex:self.curentIndex] mutableCopy];
}

- (void)saveCurentContact {
    [self.contacts replaceObjectAtIndex:self.curentIndex withObject:self.curentContact];
    [SourcesManager saveSources:self.contacts];
}

- (void)saveAllValues {    
    [self hideKeyboard];
    
    if ([self.nameTextField.text length]) {
        [self saveCurentContact];
        [SourcesManager saveImage:UIImageJPEGRepresentation(self.imageView.image, 1) contact:self.curentContact];
    }
}

- (void)hideKeyboard {
    [self.nameTextField resignFirstResponder];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (buttonIndex != 2) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        
        if (buttonIndex == 0) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } else if (buttonIndex == 1) {
            picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;        
        }
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    self.imageView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
}

- (void)setTextValues {
    self.nameTextField.text = [self.curentContact objectForKey:@"name"];
    NSString *birthdayText = [SourcesManager dateString:[self.curentContact objectForKey:@"birthday"] style:kCFDateFormatterLongStyle];
    [self.birthdayButton setTitle:birthdayText forState:UIControlStateNormal];
    
    NSString *commentText = nil;
    if ([self.curentContact objectForKey:@"comment"]) {
        commentText = [NSString stringWithFormat:@"Comment:%@", [self.curentContact objectForKey:@"comment"]];
    } else {
        commentText = @"Comment:";
    }
    
    [self.commentButton setTitle:commentText forState:UIControlStateNormal];
    
    NSString *birthdayMedium = [SourcesManager dateString:[self.curentContact objectForKey:@"birthday"] style:kCFDateFormatterMediumStyle];
    NSString *dayText = [[birthdayMedium substringWithRange:NSMakeRange(4, 2)] stringByReplacingOccurrencesOfString:@"," withString:@""];
   self. monthLabel.text = [birthdayMedium substringToIndex:3];
    self.dayLabel.text = dayText;
}

- (void)setPhotoImage {
    UIImage *photoImage = [SourcesManager loadImage:self.curentContact];
    if (photoImage) {
        self.imageView.image = photoImage;
    }
}

- (void)dateChanged:(id)sender {
    [self.curentContact setObject:[sender date] forKey:@"birthday"];
}

-(void)closeDatePicker {
    [self saveAllValues];
    [self setTextValues];
    [self.pickerViewDate dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [self.curentContact setObject:self.alertTextField.text forKey:@"comment"];
        [self saveAllValues];
        [self setTextValues];
    }
}

- (void)displayComposerSheet {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	[picker setSubject:@"Birthday"];
    
    NSInteger old = [SourcesManager calculationOld:[self.curentContact objectForKey:@"birthday"] toDate:[NSDate date]];
    NSString *emailBody = [NSString stringWithFormat:NSLocalizedString(@"emailBody", nil), self.nameTextField.text, old];
    
	[picker setMessageBody:emailBody isHTML:NO];
	
	[self presentViewController:picker animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	[self dismissViewControllerAnimated:YES completion:NULL];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
