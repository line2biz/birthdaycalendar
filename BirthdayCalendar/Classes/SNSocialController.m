//
//  DetailSocialViewController.m
//  BirthdayCalendar
//
//  Created by Alximik on 14.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SNSocialController.h"


@implementation SNSocialController

@synthesize socialName;
@synthesize syncButton;
@synthesize userNameLabel;
@synthesize logButton;


- (void)viewDidUnload
{
    self.logButton = nil;
    self.syncButton = nil;
    self.userNameLabel = nil;
    [super viewDidUnload];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *imageName = [NSString stringWithFormat:@"Sync%@.png", socialName];
    UIImage *syncImage = [UIImage imageNamed:imageName];
    [syncButton setImage:syncImage forState:UIControlStateNormal];
    
    [self nameForObjects];
}

- (void)nameForObjects {
    NSString *userNameText = [[NSUserDefaults standardUserDefaults] objectForKey:socialName];
    userNameLabel.text = userNameText;
    
    [logButton setTitle:[userNameText length]? @"Logout":@"login" forState:UIControlStateNormal];
}

- (IBAction)pressLog {
    if ([socialName isEqualToString:APInameFacebook]) {
        [[SocialWorker sharedSocialWorker] initFacebook];
        [SocialWorker sharedSocialWorker].delegate = self;
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:socialName]) {
            [[SocialWorker sharedSocialWorker] logoutFacebook];
        } else {
            [[SocialWorker sharedSocialWorker] loginToFacebook];
        }
    }
    
    [self nameForObjects];
}

- (IBAction)pressSync {
    if ([socialName isEqualToString:APInameFacebook]) {
        [[SocialWorker sharedSocialWorker] syncFacebook];
    }
    
    [self nameForObjects];
}

- (void)didLogin {
   [self nameForObjects]; 
}

- (void)didLogout {
    [self nameForObjects];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
