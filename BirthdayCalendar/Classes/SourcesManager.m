//
//  SourcesManager.m
//  ImageViewer
//
//  Created by Alximik on 18.01.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SourcesManager.h"


@implementation SourcesManager

+(void)loadSocialNetworksSettings {
    NSString *defaultPath = [[NSBundle mainBundle] pathForResource:@"SocialNetworks" ofType:@"plist"];
    NSArray *socialNetworks = [NSArray arrayWithContentsOfFile:defaultPath];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:socialNetworks forKey:SocialNetworksKey];
    
    
    NSString *finalDate = @"01-01-2011 10:00:00";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSDate *currentDateNotification = [dateFormatter dateFromString:finalDate];
    [userDefaults setObject:currentDateNotification forKey:dateNotification];
    [userDefaults setBool:YES forKey:notificationOnKey];
    [userDefaults synchronize];
}

+(NSMutableArray*)loadSources {
	
	NSMutableArray *sourcesArray = nil;
	  
	NSString *writablePath = [DOCUMENTS stringByAppendingPathComponent:@"AppContacts.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:writablePath]) {
        sourcesArray = [NSMutableArray arrayWithContentsOfFile:writablePath];
	} else {        
        sourcesArray = [NSMutableArray arrayWithCapacity:0];
        [sourcesArray writeToFile:[DOCUMENTS stringByAppendingPathComponent:@"AppContacts.plist"] atomically:YES];
    }
	
	return sourcesArray;
}

+(void)saveSources:(NSMutableArray*)sourcesArray {
	NSString *writablePath = [DOCUMENTS stringByAppendingPathComponent:@"AppContacts.plist"];
	[sourcesArray writeToFile:writablePath atomically:YES];
    [SourcesManager addPushNotifications:sourcesArray];
}

+ (void)addContactToApp:(NSMutableArray*)contacts idContact:(NSString*)idContact name:(NSString*)name picture:(NSString*)picture sourceName:(NSString*)sourceName birthday:(NSDate*)birthday {
    
    NSString *predStr = [NSString stringWithFormat:@"SELF.id == '%@'", idContact];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:predStr];
    
    if (![[contacts filteredArrayUsingPredicate:predicate] count]) {        
        NSDictionary *curentContact = [NSDictionary dictionaryWithObjectsAndKeys:idContact, @"id",
                                       name, @"name", 
                                       picture, @"picture",
                                       sourceName, @"sourceName",
                                       birthday, @"birthday", nil];
        
        [contacts addObject:curentContact];        
    }
}

+(UIImage*)loadImage:(NSDictionary*)curentContact {
    UIImage *image = nil;

    NSString *pathToFile = [DOCUMENTS stringByAppendingPathComponent:[self photoName:curentContact]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:pathToFile]) {
        NSData *imageData = [NSData dataWithContentsOfFile:pathToFile];
        image = [UIImage imageWithData:imageData];
    }
    
    return image;
}

+(void)saveImage:(NSData*)dataImage contact:(NSDictionary*)curentContact {
	NSString *docPath = [DOCUMENTS stringByAppendingPathComponent:[self photoName:curentContact]];		
    
	if (![[NSFileManager defaultManager] createFileAtPath:docPath contents:dataImage attributes:nil]) {
		NSLog(@"Error saving");
	}
}

+(void)saveImageForName:(NSData*)dataImage name:(NSString*)name {
  	NSString *docPath = [DOCUMENTS stringByAppendingPathComponent:name];		
    
	if (![[NSFileManager defaultManager] createFileAtPath:docPath contents:dataImage attributes:nil]) {
		NSLog(@"Error saving");
	}  
}

+ (NSString*)photoName:(NSDictionary*)curentContact {
    NSString *photoName = [NSString stringWithFormat:@"%@_%@.jpg", 
                           [curentContact objectForKey:@"sourceName"], 
                           [curentContact objectForKey:@"id"]];
    return photoName;
}

+ (void)roundView:(UIView*)view {
    CALayer *layer = [view layer];
    layer.masksToBounds = YES;
    layer.cornerRadius = 5.0f;
    layer.borderWidth = 0.0f;
}

+ (NSString*)dateString:(NSDate*)date style:(CFDateFormatterStyle)dateStyle {
    [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:dateStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    return [dateFormatter stringFromDate:date];
}

+ (NSString*)timeString:(NSDate*)date {
    [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateStyle:kCFDateFormatterNoStyle];
    [dateFormatter setTimeStyle:kCFDateFormatterShortStyle];
    return [dateFormatter stringFromDate:date];
}

+ (NSDate*)currentBirthday:(NSDate*)birthday inYear:(NSDate*)year {
    NSDateFormatter *dateFormat=[NSDateFormatter new];
    
    NSDateComponents *comps=[NSDateComponents new];
    [comps setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    [dateFormat setDateFormat:@"dd"];
    [comps setDay:[[dateFormat stringFromDate:birthday] intValue]];
    [dateFormat setDateFormat:@"MM"];
    [comps setMonth:[[dateFormat stringFromDate:birthday] intValue]];
    [dateFormat setDateFormat:@"yyyy"];
    [comps setYear:[[dateFormat stringFromDate:year] intValue]];
    
    NSDate *currentContactBirthday=[[NSCalendar currentCalendar] dateFromComponents:comps];
    return currentContactBirthday;
}

+ (NSDate*)nextBirthday:(NSDate*)birthday {
    NSDate *currentContactBirthday=[self currentBirthday:birthday inYear:[NSDate date]];
    NSDate *earlierDate = [currentContactBirthday earlierDate:[NSDate date]];
    if ([earlierDate isEqualToDate:currentContactBirthday]) {
        currentContactBirthday = [self currentBirthday:birthday inYear:newYear];
    }
    return currentContactBirthday;
}

+ (NSDate*)fireDate:(NSDate*)birthday {
    NSDate *currentDateNotification = [[NSUserDefaults standardUserDefaults] objectForKey:dateNotification];
    
    NSDateFormatter *dateFormat=[NSDateFormatter new];
    NSDateComponents *comps=[NSDateComponents new];
    [comps setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    
    [dateFormat setDateFormat:@"dd"];
    [comps setDay:[[dateFormat stringFromDate:birthday] intValue]];
    [dateFormat setDateFormat:@"MM"];
    [comps setMonth:[[dateFormat stringFromDate:birthday] intValue]];
    [dateFormat setDateFormat:@"yyyy"];
    [comps setYear:[[dateFormat stringFromDate:birthday] intValue]];
    [dateFormat setDateFormat:@"HH"];
    [comps setHour:[[dateFormat stringFromDate:currentDateNotification] intValue]];
    [dateFormat setDateFormat:@"mm"];
    [comps setMinute:[[dateFormat stringFromDate:currentDateNotification] intValue]];

    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

+ (void)cleanOldNotifications {
    UIApplication *app                = [UIApplication sharedApplication];
    NSArray *oldNotifications         = [app scheduledLocalNotifications];
    
    if ([oldNotifications count] > 0) {
        [app cancelAllLocalNotifications];
    }
}

+ (void)addPushNotifications:(NSArray*)contacts
{
    
    [self cleanOldNotifications];
    
    UIApplication *app = [UIApplication sharedApplication];

    NSMutableArray *birthdayPeople = [NSMutableArray array];
    
    for (NSDictionary *contact in contacts) {
        if ([contact objectForKey:@"birthday"]) {
            NSDate *currentContactBirthday = [self nextBirthday:[contact objectForKey:@"birthday"]];
            
            NSDictionary *birthdayContact = [NSDictionary dictionaryWithObjectsAndKeys:
                                             currentContactBirthday, @"birthdayInFuture", 
                                             [contact objectForKey:@"name"], @"name", 
                                             [contact objectForKey:@"birthday"], @"birthday", nil];
            [birthdayPeople addObject:birthdayContact];
        }        
    }
    
    for (NSDate* day=[NSDate date]; [day isEqualToDate:[day earlierDate:newYear]]; day=[day dateByAddingTimeInterval:60.0f*60.0f*24.0f]) {
        day = [self currentBirthday:day inYear:day];
    	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.birthdayInFuture >= %@ && SELF.birthdayInFuture <= %@", day, day];
        NSArray *birthdayPeopleInOneDay = [birthdayPeople filteredArrayUsingPredicate:predicate];
        
        if ([birthdayPeopleInOneDay count]) {
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.timeZone  = [NSTimeZone systemTimeZone];
            notification.fireDate  = [self fireDate:day];
            notification.alertBody = [self formAlertBody:birthdayPeopleInOneDay];
            [app scheduleLocalNotification:notification];
        }
    }
    
}

+ (NSString*)formAlertBody:(NSArray*)contacts {
    NSString *alertBody = @"";
    
    if ([contacts count] == 1) {
        NSDictionary *contact = [contacts objectAtIndex:0];
        NSString *name = [contact objectForKey:@"name"];
        NSInteger old = [self calculationOld:[contact objectForKey:@"birthday"] toDate:[contact objectForKey:@"birthdayInFuture"]];
        alertBody = [NSString stringWithFormat:NSLocalizedString(@"alertBodyOne", nil), name, old];
    } else if ([contacts count] > 1) {
        for (NSDictionary *contact in contacts) {
            alertBody = [alertBody stringByAppendingFormat:@"\"%@\", ", [contact objectForKey:@"name"]];
        }
        alertBody = [alertBody substringToIndex:[alertBody length]-2];
        alertBody = [NSString stringWithFormat:NSLocalizedString(@"alertBodyMany", nil), alertBody];
    }
    
    return alertBody;
}

+ (NSInteger)calculationOld:(NSDate*)birthday toDate:(NSDate*)toDate {
    NSDateFormatter *dateFormat=[NSDateFormatter new];
    [dateFormat setDateFormat:@"yyyy"];
    
    NSInteger old = [[dateFormat stringFromDate:toDate] intValue] - [[dateFormat stringFromDate:birthday] intValue];
    return old;
}

@end
