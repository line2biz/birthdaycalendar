//
//  EditSourcesCell.h
//  ImageViewer
//
//  Created by Alximik on 05.01.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditSourcesCell : UITableViewCell {

	UILabel *nameSources;
    UILabel *time;
	UISwitch *mySwitch;
	
}

@property (nonatomic, strong) IBOutlet UILabel *nameSources;
@property (nonatomic, strong) IBOutlet UILabel *time;
@property (nonatomic, strong) IBOutlet UISwitch *mySwitch;

@end
