//
//  SettingsViewController.h
//  BirthdayCalendar
//
//  Created by Alximik on 04.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EditSourcesCell.h"
#import "SNSocialController.h"


@interface SNSettingsController : UITableViewController <UIActionSheetDelegate> {
	NSMutableArray *sourcesArray;
    UIActionSheet *pickerViewDate;
    NSDate *currentDateNotification;
}

@property (nonatomic, strong) NSMutableArray *sourcesArray;
@property (nonatomic, strong) UIActionSheet *pickerViewDate;
@property (nonatomic, strong) NSDate *currentDateNotification;

- (void)switchDidChange:(id)sender;

@end
