//
//  HomeViewController.m
//  BirthdayCalendar
//
//  Created by Alximik on 04.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SNHomeController.h"

@interface SNHomeController () <SocialWorkerlDelegate>

@end

@implementation SNHomeController


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *logoimage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BarLogo"]];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoimage];
}

#pragma mark - Outlet Methods
- (IBAction)pressSyncFacebook {
    NSMutableArray *sourcesArray = [[[NSUserDefaults standardUserDefaults] objectForKey:SocialNetworksKey] mutableCopy];
	
	NSMutableDictionary *sourcesObjects = [[sourcesArray objectAtIndex:0] mutableCopy];
	[sourcesObjects setObject:@"YES" forKey:@"activated"];
	[sourcesArray replaceObjectAtIndex:0 withObject:sourcesObjects];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:sourcesArray forKey:SocialNetworksKey];
    [userDefaults synchronize];	
    
    [SocialWorker sharedSocialWorker].delegate = self;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:APInameFacebook]) {
        [[SocialWorker sharedSocialWorker] syncFacebook];        
    } else {
        [[SocialWorker sharedSocialWorker] loginToFacebook];
    }
}


- (void)didLogin
{
    [[SocialWorker sharedSocialWorker] syncFacebook]; 
}

- (IBAction)pressSyncAdressBook
{
    [[SocialWorker sharedSocialWorker] syncAdressBook];
}


@end
