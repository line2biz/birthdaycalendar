//
//  CalendarViewController.m
//  BirthdayCalendar
//
//  Created by Alximik on 06.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//



#import "SNCalendarController.h"


#import "SNProfileContriller.h"

@implementation SNCalendarController

@synthesize dataArray;
@synthesize dataDictionary;
@synthesize contacts;


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *logoimage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BarLogo.png"]];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logoimage];
    
    [self.monthView selectDate:[NSDate date]];
    self.tableView.rowHeight = tableRowHeight;
    CGRect tableFrame = self.tableView.frame;
    tableFrame.size.height = tableFrame.size.height-49.0f;
    self.tableView.frame = tableFrame;
    
}

- (void)viewWillAppear:(BOOL)animated {
    [self.monthView reload];
    [self.tableView reloadData];
}

#pragma mark - Calendar

- (NSArray*)calendarMonthView:(TKCalendarMonthView*)monthView marksFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate{
	[self createDataForCalendar:startDate endDate:lastDate];
	return dataArray;
}
- (void)calendarMonthView:(TKCalendarMonthView*)monthView didSelectDate:(NSDate*)date{
	[self.tableView reloadData];
}
- (void) calendarMonthView:(TKCalendarMonthView*)mv monthDidChange:(NSDate*)d animated:(BOOL)animated{
	[super calendarMonthView:mv monthDidChange:d animated:animated];
	[self.tableView reloadData];
}

- (void)createDataForCalendar:(NSDate*)start endDate:(NSDate*)end{
	
	//NSLog(@"Delegate Range: %@ %@ %d",start,end,[start daysBetweenDate:end]);
    self.contacts = [SourcesManager loadSources];
    self.dataArray = [NSMutableArray array];
	self.dataDictionary = [NSMutableDictionary dictionary];

	NSDate *startDate = start;
	while(YES){
		
        NSMutableArray *birthdays = [NSMutableArray array];
        for (NSDictionary *contact in contacts) {
            if ([contact objectForKey:@"birthday"]) {
                NSDate *currentContactBirthday=[SourcesManager currentBirthday:[contact objectForKey:@"birthday"] inYear:[NSDate date]];
                if ([currentContactBirthday isEqualToDate:startDate]) {
                    [birthdays addObject:contact];
                }
            }
        }
        
        if ([birthdays count]) {
            [self.dataDictionary setObject:birthdays forKey:startDate];
            [self.dataArray addObject:[NSNumber numberWithBool:YES]];
        } else {
            [self.dataArray addObject:[NSNumber numberWithBool:NO]];
        }        
        
		TKDateInformation info = [startDate dateInformationWithTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		info.day++;
		startDate = [NSDate dateFromDateInformation:info timeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
		if([startDate compare:end]==NSOrderedDescending) break;
	}
	
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {	
    NSArray *tableSource = [dataDictionary objectForKey:[self.monthView dateSelected]];
     return tableSource? [tableSource count]:0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
	NSArray *tableSource = [dataDictionary objectForKey:[self.monthView dateSelected]];
    NSDictionary *curentContact = [tableSource objectAtIndex:indexPath.row];
    cell.textLabel.text = [curentContact objectForKey:@"name"];
    cell.detailTextLabel.text = [SourcesManager dateString:[curentContact objectForKey:@"birthday"] style:kCFDateFormatterLongStyle];
    
    UIImage *photoImage = [self photoForContact:curentContact];
    cell.imageView.image = [photoImage imageScaledToFitSize:CGSizeMake(50.0f, 50.0f)];
    
    [SourcesManager roundView:cell.imageView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = NO;
    
    NSArray *tableSource = [dataDictionary objectForKey:[self.monthView dateSelected]];
    NSDictionary *curentContact = [tableSource objectAtIndex:indexPath.row];
    
    SNProfileContriller *profileContriller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SNProfileContriller class])];
    profileContriller.editing = NO;
    profileContriller.curentIndex = [contacts indexOfObject:curentContact];
    profileContriller.curentContact = [curentContact mutableCopy];
    [self.navigationController pushViewController:profileContriller animated:YES];
}

- (UIImage*)photoForContact:(NSDictionary*)curentContact {
    UIImage *photo = [SourcesManager loadImage:curentContact];
    
    if (!photo) {
        NSString *imageURL = [curentContact valueForKeyPath:@"picture.data.url"];
        if ([imageURL length]) {
            [self startDownloadFile:imageURL index:[contacts indexOfObject:curentContact]];
        } else {
            photo = [UIImage imageNamed:@"notPhoto.png"];
        }
    }
    
    return photo;
}

- (void)startDownloadFile:(NSString*)URL index:(NSInteger)tag {
    APICall *request = [APICall callWithURLString:URL delegate:self];
    request.tag = tag;
    [request start];
}

- (NSString*)photoName:(NSDictionary*)curentContact {
    NSString *photoName = [NSString stringWithFormat:@"%@_%@.jpg", 
                           [curentContact objectForKey:@"sourceName"], 
                           [curentContact objectForKey:@"id"]];
    return photoName;
}

- (void) APICall:(APICall *)request didLoadValue:(NSData *)data {
    NSDictionary *curentContact = [contacts objectAtIndex:request.tag];
    [SourcesManager saveImage:data contact:curentContact];
    
    [self.tableView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
