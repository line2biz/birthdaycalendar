//
//  SNAppDelegate.h
//  BirthdayCalendar
//
//  Created by Шурик on 10.03.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
