//
//  main.m
//  BirthdayCalendar
//
//  Created by Шурик on 10.03.14.
//  Copyright (c) 2014 Alexandr Zhovty. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SNAppDelegate class]));
    }
}
